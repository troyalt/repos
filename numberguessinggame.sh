#! /bin/bash
n1=$[ ($RANDOM % 11)]
guesses=1
echo -n "I'm thinking of a number between 1 and 10. Guess that number "

while read n2; do

if [[ $n2 -eq $n1 ]]; then break;
else
echo
if [[ $n2 -gt $n1 ]]; then
echo -n "your guess is too high, try again"
elif [[ $n2 -lt $n1 ]]; then
echo -n "your guess is too low, try again"
fi
fi
guesses=$((guesses+1))

done
echo
echo "congratulations! It took you $guesses guess(es) to get the right number, which is $n1"
