#! /bin/bash
for filetypes in "*.txt" "*.csv" "*.dat" "*.jpeg" "*.sh"
do
echo "$filetypes"
ls | egrep  "$filetypes" | wc -l
done
