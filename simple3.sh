#! /bin/bash/
#usage: put in quotes what file type you want to change then add another quotes with what you want to change to or leave blank for old
for filename in $1
do
if [ -z $2 ]
then
newentry='old'"$(echo "$filename")"
mv "$filename" "$newentry"
else
newentry=$2"$(echo "$filename")"
mv "$filename" "$newentry"
fi
done
